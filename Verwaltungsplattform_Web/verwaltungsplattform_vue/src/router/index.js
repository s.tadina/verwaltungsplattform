import {createRouter, createWebHistory} from 'vue-router'
import DashboardView from "../views/DashboardView.vue"
import TrainerView from "../views/TrainerView.vue"
import StudentView from "../views/StudentView.vue"
import GroupView from "../views/GroupView.vue"
import SingleTrainerView from "@/views/SingleTrainerView.vue"
import SingleStudentView from "@/views/SingleStudentView.vue"
import SingleGroupView from "@/views/SingleGroupView.vue"
import RoomView from "@/views/RoomView.vue";
import SingleRoomView from "@/views/SingleRoomView.vue"
import LessonView from "@/views/LessonView.vue";
import SingleLessonView from "@/views/SingleLessonView.vue"
import LoginView from "@/views/LoginView.vue"


const routes = [
    {path: "/dashboard", component: DashboardView},
    {path: "/trainer", component: TrainerView},
    {path: "/student", component: StudentView},
    {path: "/group", component: GroupView},
    {path: "/room", component: RoomView},
    {path: "/lesson", component: LessonView},
    {path: "/trainer/:trainerIndex", component: SingleTrainerView},
    {path: "/student/:studentIndex", component: SingleStudentView},
    {path: "/group/:groupIndex", component: SingleGroupView},
    {path: "/room/:roomIndex", component: SingleRoomView},
    {path: "/lesson/:lessonIndex", component: SingleLessonView},
    {path: "/login", component: LoginView}


]

const router = createRouter({
    routes,
    history: createWebHistory()
})
export
{
    router
}