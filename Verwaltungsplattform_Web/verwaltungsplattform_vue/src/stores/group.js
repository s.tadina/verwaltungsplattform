import {defineStore} from "pinia";
import axios from "axios";


export const useGroupStore = defineStore("group", {
    state: () => ({
        groups: []
    }),
    actions: {
        async createGroup(group) {
            const response = await axios.post("/api/groups", group)
            this.groups.push(response.data)
            return response.data
        },
        async updateGroup(group) {
            const response = await axios.put("/api/groups/" + group.groupId, group)
            this.groups.splice(this.groups.indexOf(this.groups.find(groupArr => response.data.id === groupArr.groupId)), 1)
            this.groups.push(response.data)

            return response
        },
        async deleteGroup(group) {
            const response = await axios.delete("/api/groups/" + group.groupId)
            this.groups.splice(this.groups.indexOf(this.groups.find(groupArr => groupArr.groupId === group.groupId)), 1)
            return response
        }
    }

})