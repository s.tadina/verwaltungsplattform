import {defineStore} from "pinia";
import axios from "axios";

export const useLessonStore = defineStore("lesson", {
    state: () => ({
        lessons: []
    }),
    actions: {
        async createLesson(lesson) {
            const response = await axios.post("/api/lessons", lesson)
            this.lessons.push(response.data)
            return response
        },
        async updateLesson(lesson) {
            const response = await axios.put("/api/lessons/" + lesson.lessonId, lesson)
            this.lessons.splice(this.lessons.indexOf(this.lessons.find(lesson => response.data.id === lesson.lessonId)), 1)
            this.lessons.push(response.data)
            return response
        },
        async deleteLesson(lesson) {
            const response = await axios.delete("/api/lessons/" + lesson.lessonId)
            this.lessons.splice(this.lessons.indexOf(this.lessons.find(lessonArr => lessonArr.lessonId === lesson.lessonId)), 1)
            return response
        }
    }

})