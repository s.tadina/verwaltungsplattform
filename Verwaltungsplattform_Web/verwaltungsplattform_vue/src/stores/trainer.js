import {defineStore} from "pinia";
import axios from "axios";

export const useTrainerStore = defineStore("trainer", {
    state: () => ({
        trainers: []
    }),
    actions: {
        async createTrainer(trainer) {
            const response = await axios.post("/api/trainers", trainer)
            this.trainers.push(response.data)
            return response
        },
        async updateTrainer(trainer) {
            const response = await axios.put("/api/trainers/" + trainer.id, trainer)
            this.trainers.splice(this.trainers.indexOf(this.trainers.find(trainer => response.data.id === trainer.id)), 1)
            this.trainers.push(response.data)
            return response
        },
        async deleteTrainer(trainer) {
            const response = await axios.delete("/api/trainers/" + trainer.id)
            this.trainers.splice(this.trainers.indexOf(this.trainers.find(trainerArr => trainerArr.id === trainer.id)), 1)
            return response
        }
    }
})
