import {defineStore} from "pinia";
import axios from "axios";

export const useStudentStore = defineStore("student", {
    state: () => ({
        students: []
    }),
    actions: {
        async createStudent(student) {
            const response = await axios.post("/api/students", student)
            this.students.push(response.data)
            return response
        },
        async updateStudent(student) {
            const response = await axios.put("/api/students/" + student.id, student)
            this.students.splice(this.students.indexOf(this.students.find(student => response.data.id === student.id)), 1)
            this.students.push(response.data)
            return response
        },
        async deleteStudent(student) {
            const response = await axios.delete("/api/students/" + student.id)
            this.students.splice(this.students.indexOf(this.students.find(studentArr => studentArr.id === student.id)), 1)
            return response
        }
    }
})