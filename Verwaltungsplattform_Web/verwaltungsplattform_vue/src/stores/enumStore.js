import {defineStore} from "pinia";


export const useEnumStore = defineStore("enumStore", {
    state: () => ({
        specializations: [],
        subjects: [],
        employmentTypes: [],
        weekdays: []
    })
})
