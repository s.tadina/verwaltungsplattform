import {defineStore} from "pinia";
import axios from "axios";

export const useRoomStore = defineStore("room", {
    state: () => ({
        rooms: []
    }),
    actions: {
        async createRoom(room) {
            const response = await axios.post("/api/rooms", room)
            this.rooms.push(response.data)
            return response
        },
        async updateRoom(room) {
            const response = await axios.put("/api/rooms/" + room.roomId, room)
            this.rooms.splice(this.rooms.indexOf(this.rooms.find(room => response.data.id === room.roomId)), 1)
            this.rooms.push(response.data)
            return response
        },
        async deleteRoom(room) {
            const response = await axios.delete("/api/rooms/" + room.roomId)
            this.rooms.splice(this.rooms.indexOf(this.rooms.find(roomArr => roomArr.roomId === room.roomId)), 1)
            return response
        }
    }

})