package com.example.Verwaltungsplattform.services;

import com.example.Verwaltungsplattform.DTOs.TrainerDTO;
import com.example.Verwaltungsplattform.enums.EmploymentType;
import com.example.Verwaltungsplattform.enums.Subject;
import com.example.Verwaltungsplattform.model.TrainerModel;
import com.example.Verwaltungsplattform.repository.LessonRepository;
import com.example.Verwaltungsplattform.repository.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class TrainerService {
    @Autowired
    TrainerRepository trainerRepository;
    @Autowired
    LessonRepository lessonRepository;

    public double calculateNetSalary(int trainerId) {
        TrainerModel trainer = trainerRepository.findById(trainerId).get();

        double netSalary = trainer.getGrossSalary() * 0.8;

        return netSalary;
    }

    public TrainerDTO post(TrainerDTO trainerDTO) {

        TrainerModel trainerModel = new TrainerModel();

        trainerModel.setFirstName(trainerDTO.getFirstName());
        trainerModel.setLastName(trainerDTO.getLastName());
        trainerModel.setEmail(trainerDTO.getEmail());
        trainerModel.setResidence(trainerDTO.getResidence());
        trainerModel.setGrossSalary(trainerDTO.getSalary());
        trainerModel.setEmploymentType(EmploymentType.stringToEnum(trainerDTO.getEmploymentType()));

        trainerRepository.save(trainerModel);

        Set<Subject> specializationSet = new HashSet<>();
        trainerDTO.getSubjects().forEach(specialization -> specializationSet.add(Subject.stringToSubject(specialization)));
        trainerModel.setSubject(specializationSet);

        trainerRepository.save(trainerModel);

        Set<String> subjects = new HashSet<>();
        trainerModel.getSubject().forEach(sub -> subjects.add(sub.getName()));
        return new TrainerDTO()
                .setId(trainerModel.getId())
                .setFirstName(trainerModel.getFirstName())
                .setLastName(trainerModel.getLastName())
                .setEmail(trainerModel.getEmail())
                .setResidence(trainerModel.getResidence())
                .setSalary(trainerModel.getGrossSalary())
                .setEmploymentType(trainerModel.getEmploymentType() != null ? trainerModel.getEmploymentType().getName() : "")
                .setSubjects(subjects);
    }

    public TrainerDTO put(TrainerDTO trainerDTO) {

        TrainerModel trainerModel = trainerRepository.findById(trainerDTO.getId()).get();

        Set<Subject> subjects = new HashSet<>();

        trainerModel.setFirstName(trainerDTO.getFirstName());
        trainerModel.setLastName(trainerDTO.getLastName());
        trainerModel.setEmail(trainerDTO.getEmail());
        trainerModel.setResidence(trainerDTO.getResidence());
        trainerModel.setGrossSalary(trainerDTO.getSalary());
        trainerModel.setEmploymentType(EmploymentType.stringToEnum(trainerDTO.getEmploymentType()));

        trainerRepository.save(trainerModel);

        for (String sub : trainerDTO.getSubjects()) {
            subjects.add(Subject.stringToSubject(sub));
            System.err.println(sub);
        }

        trainerModel.setSubject(subjects);

        trainerRepository.save(trainerModel);

        Set<String> subjectString = new HashSet<>();
        trainerModel.getSubject().forEach(subject -> subjectString.add(subject.getName()));

        return new TrainerDTO()
                .setId(trainerModel.getId())
                .setFirstName(trainerModel.getFirstName())
                .setLastName(trainerModel.getLastName())
                .setEmail(trainerModel.getEmail())
                .setResidence(trainerModel.getResidence())
                .setSalary(trainerModel.getGrossSalary())
                .setEmploymentType(trainerModel.getEmploymentType() != null ? trainerModel.getEmploymentType().getName() : "")
                .setSubjects(subjectString);
    }

    public void delete(int trainerId) {
        trainerRepository.deleteById(trainerId);
    }


}
