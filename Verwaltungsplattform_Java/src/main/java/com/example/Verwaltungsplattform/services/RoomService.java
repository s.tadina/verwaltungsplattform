package com.example.Verwaltungsplattform.services;

import com.example.Verwaltungsplattform.DTOs.RoomDTO;
import com.example.Verwaltungsplattform.model.GroupModel;
import com.example.Verwaltungsplattform.model.RoomModel;
import com.example.Verwaltungsplattform.repository.GroupRepository;
import com.example.Verwaltungsplattform.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomService {

    @Autowired
    RoomRepository roomRepository;
    @Autowired
    GroupRepository groupRepository;


    public RoomDTO post(RoomDTO roomDTO) {

        RoomModel roomModel = new RoomModel();

        roomModel.setRoomNumber(roomDTO.getRoomNumber());
        roomModel.setVip(roomDTO.getVip());
        roomModel.setDescriptionRoom(roomDTO.getDescriptionRoom());


        roomRepository.save(roomModel);

        return new RoomDTO()
                .setRoomId(roomModel.getRoomId())
                .setRoomNumber(roomModel.getRoomNumber())
                .setVip(roomModel.getVip())
                .setDescriptionRoom(roomModel.getDescriptionRoom());

    }

    public RoomDTO put(RoomDTO roomDTO, int roomId) {

        RoomModel roomModel = roomRepository.findById(roomId).get();

        roomModel.setRoomNumber(roomDTO.getRoomNumber());
        roomModel.setVip(roomDTO.getVip());
        roomModel.setDescriptionRoom(roomDTO.getDescriptionRoom());


        roomRepository.save(roomModel);

        return new RoomDTO()
                .setRoomId(roomModel.getRoomId())
                .setRoomNumber(roomModel.getRoomNumber())
                .setVip(roomModel.getVip())
                .setDescriptionRoom(roomModel.getDescriptionRoom());

    }

    public void delete(int roomId) {
        roomRepository.deleteById(roomId);
    }
}
