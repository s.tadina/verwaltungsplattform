package com.example.Verwaltungsplattform.services;

import com.example.Verwaltungsplattform.DTOs.LoginDTO;
import com.example.Verwaltungsplattform.model.LoginModel;
import com.example.Verwaltungsplattform.repository.LoginRepository;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    final LoginRepository loginRepository;

    public LoginService(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    public boolean login(LoginDTO loginDTO) {
        if (loginRepository.existsById(loginDTO.getUsername())) {
            LoginModel loginModel = loginRepository.findById(loginDTO.getUsername()).get();
            return loginModel.getPassword() == loginDTO.getPassword().hashCode();
        }
        return false;
    }

    public boolean createLogin(LoginDTO loginDTO) {
        LoginModel loginModel = new LoginModel();
        loginModel.setUsername(loginDTO.getUsername());
        loginModel.setPassword(loginDTO.getPassword().hashCode());
        loginRepository.save(loginModel);
        return true;
    }

}
