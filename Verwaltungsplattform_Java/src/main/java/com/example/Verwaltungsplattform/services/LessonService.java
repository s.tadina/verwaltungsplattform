package com.example.Verwaltungsplattform.services;

import com.example.Verwaltungsplattform.DTOs.LessonDTO;
import com.example.Verwaltungsplattform.enums.Subject;
import com.example.Verwaltungsplattform.enums.Weekdays;
import com.example.Verwaltungsplattform.model.*;
import com.example.Verwaltungsplattform.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class LessonService {
    @Autowired
    LessonRepository lessonRepository;
    @Autowired
    TrainerRepository trainerRepository;
    @Autowired
    GroupRepository groupRepository;


    public LessonDTO post(LessonDTO lessonDTO) {

        LessonModel lessonModel = new LessonModel();


        lessonModel.setTrainerModel(trainerRepository.findById(lessonDTO.getTrainerId()).orElse(null));
        lessonModel.setGroupModel(groupRepository.findById(lessonDTO.getGroupId()).orElse(null));
        lessonModel.setSubjects(Subject.stringToSubject(lessonDTO.getSubjects()));
        lessonModel.setWeekday(Weekdays.stringToEnum(lessonDTO.getWeekday()));


        lessonRepository.save(lessonModel);


        return new LessonDTO()
                .setLessonId(lessonModel.getLessonId())
                .setTrainerId(lessonModel.getTrainerModel() != null ? lessonModel.getTrainerModel().getId() : -1)
                .setGroupId(lessonModel.getGroupModel() != null ? lessonModel.getGroupModel().getGroupId() : -1)
                .setSubjects(lessonModel.getSubjects().getName())
                .setWeekday(lessonModel.getWeekday().getName());
    }

    public LessonDTO put(LessonDTO lessonDTO, int lessonId) {

        LessonModel lessonModel = lessonRepository.findById(lessonId).get();

        lessonModel.setTrainerModel(trainerRepository.findById(lessonDTO.getTrainerId()).orElse(null));
        lessonModel.setGroupModel(groupRepository.findById(lessonDTO.getGroupId()).orElse(null));
        lessonModel.setSubjects(Subject.stringToSubject(lessonDTO.getSubjects()));
        lessonModel.setWeekday(Weekdays.stringToEnum(lessonDTO.getWeekday()));


        lessonRepository.save(lessonModel);


        return new LessonDTO()
                .setLessonId(lessonModel.getLessonId())
                .setTrainerId(lessonModel.getTrainerModel() != null ? lessonModel.getTrainerModel().getId() : -1)
                .setGroupId(lessonModel.getGroupModel() != null ? lessonModel.getGroupModel().getGroupId() : -1)
                .setSubjects(lessonModel.getSubjects().getName())
                .setWeekday(lessonModel.getWeekday().getName());
    }

    public void delete(int lessonId) {
        lessonRepository.deleteById(lessonId);
    }

}
