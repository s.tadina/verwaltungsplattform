package com.example.Verwaltungsplattform.services;

import com.example.Verwaltungsplattform.DTOs.GroupDTO;
import com.example.Verwaltungsplattform.enums.Specializations;
import com.example.Verwaltungsplattform.model.GroupModel;
import com.example.Verwaltungsplattform.model.LessonModel;
import com.example.Verwaltungsplattform.model.RoomModel;
import com.example.Verwaltungsplattform.model.StudentModel;
import com.example.Verwaltungsplattform.repository.GroupRepository;
import com.example.Verwaltungsplattform.repository.LessonRepository;
import com.example.Verwaltungsplattform.repository.RoomRepository;
import com.example.Verwaltungsplattform.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GroupService {

    @Autowired
    GroupRepository groupRepository;
    @Autowired
    RoomRepository roomRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    LessonRepository lessonRepository;


    public GroupDTO post(GroupDTO groupDTO) {

        GroupModel groupModel = new GroupModel();


        groupModel.setGroupNumber(groupDTO.getGroupNumber());
        groupModel.setSpecializations(Specializations.stringToEnum(groupDTO.getSpecializations()));
        groupModel.setStartDate(groupDTO.getStartDate());
        groupModel.setEndDate(groupDTO.getEndDate());
        groupModel.setRoomModel(roomRepository.findById(groupDTO.getRoomId()).orElse(null));

        groupRepository.save(groupModel);

        return new GroupDTO()
                .setGroupId(groupModel.getGroupId())
                .setGroupNumber(groupModel.getGroupNumber())
                .setSpecializations(groupModel.getSpecializations() != null ? groupModel.getSpecializations().getName() : "")
                .setStartDate(groupModel.getStartDate())
                .setEndDate(groupModel.getEndDate())
                .setRoomId(groupModel.getRoomModel() != null ? groupModel.getRoomModel().getRoomId() : -1);
    }

    public GroupDTO put(GroupDTO groupDTO, int groupId) {

        GroupModel groupModel = groupRepository.findById(groupId).get();

        groupModel.setGroupNumber(groupDTO.getGroupNumber());
        groupModel.setSpecializations(Specializations.stringToEnum(groupDTO.getSpecializations()));
        groupModel.setStartDate(groupDTO.getStartDate());
        groupModel.setEndDate(groupDTO.getEndDate());
        groupModel.setRoomModel(roomRepository.findById(groupDTO.getRoomId()).orElse(null));

        groupRepository.save(groupModel);

        return new GroupDTO()
                .setGroupId(groupModel.getGroupId())
                .setGroupNumber(groupModel.getGroupNumber())
                .setSpecializations(groupModel.getSpecializations() != null ? groupModel.getSpecializations().getName() : "")
                .setStartDate(groupModel.getStartDate())
                .setEndDate(groupModel.getEndDate())
                .setRoomId(groupModel.getRoomModel() != null ? groupModel.getRoomModel().getRoomId() : -1);
    }

    public void delete(int groupId) {
        groupRepository.deleteById(groupId);
    }


}
