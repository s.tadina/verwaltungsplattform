package com.example.Verwaltungsplattform.services;

import com.example.Verwaltungsplattform.DTOs.*;
import com.example.Verwaltungsplattform.enums.EmploymentType;
import com.example.Verwaltungsplattform.enums.Specializations;
import com.example.Verwaltungsplattform.enums.Subject;
import com.example.Verwaltungsplattform.enums.Weekdays;
import com.example.Verwaltungsplattform.model.*;
import com.example.Verwaltungsplattform.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AllDataService {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    LessonRepository lessonRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    TrainerRepository trainerRepository;

    public AllDataDTO getAllData() {
        Set<GroupDTO> groupDTOSet = new HashSet<>();
        Set<LessonDTO> lessonDTOSet = new HashSet<>();
        Set<RoomDTO> roomDTOSet = new HashSet<>();
        Set<StudentDTO> studentDTOSet = new HashSet<>();
        Set<TrainerDTO> trainerDTOSet = new HashSet<>();

        for (GroupModel groupModel : groupRepository.findAll()) {
            Set<Integer> studentIdSet = new HashSet<>();
            Set<Integer> lessonIdSet = new HashSet<>();
            for (LessonModel lessonModel : groupModel.getLessonModelSet()) {
                lessonIdSet.add(lessonModel.getLessonId());
            }

            for (StudentModel studentModel : groupModel.getStudentModelSet())
                studentIdSet.add(studentModel.getId());


            groupDTOSet.add(new GroupDTO(

                    groupModel.getGroupId(),
                    groupModel.getGroupNumber(),
                    groupModel.getSpecializations().getName(),
                    groupModel.getRoomModel() != null ? groupModel.getRoomModel().getRoomId() : -1,
                    groupModel.getStartDate(),
                    groupModel.getEndDate(),
                    lessonIdSet,
                    studentIdSet
            ));
        }
        for (LessonModel lessonModel : lessonRepository.findAll()) {


            lessonDTOSet.add(new LessonDTO(
                    lessonModel.getLessonId(),
                    lessonModel.getTrainerModel() != null ? lessonModel.getTrainerModel().getId() : -1,
                    lessonModel.getGroupModel() != null ? lessonModel.getGroupModel().getGroupId() : -1,
                    lessonModel.getSubjects().getName(),
                    lessonModel.getWeekday().getName()
            ));
        }
        for (RoomModel roomModel : roomRepository.findAll()) {
            roomDTOSet.add(new RoomDTO(
                    roomModel.getRoomId(),
                    roomModel.getRoomNumber(),
                    roomModel.getDescriptionRoom(),
                    roomModel.getVip(),
                    roomModel.getGroupModel() != null ? roomModel.getGroupModel().getGroupId() : -1
            ));
        }
        for (StudentModel studentModel : studentRepository.findAll()) {
            studentDTOSet.add(new StudentDTO(
                    studentModel.getFirstName(),
                    studentModel.getLastName(),
                    studentModel.getEmail(),
                    studentModel.getSpecializations().getName(),
                    studentModel.getId(),
                    studentModel.getGroupModel() != null ? studentModel.getGroupModel().getGroupId() : -1
            ));
        }

        for (TrainerModel trainerModel : trainerRepository.findAll()) {

            Set<String> subject = new HashSet<>();
            trainerModel.getSubject().forEach(sub -> subject.add(sub.getName()));

            trainerDTOSet.add(new TrainerDTO(
                    trainerModel.getFirstName(),
                    trainerModel.getLastName(),
                    trainerModel.getEmail(),
                    trainerModel.getResidence(),
                    trainerModel.getEmploymentType().getName(),
                    trainerModel.getGrossSalary(),
                    new HashSet<>(),
                    trainerModel.getId(),
                    subject
            ));
        }

        List<String> specializations = new ArrayList<>();
        for (Specializations specializations1 : Specializations.values()) {
            specializations.add(specializations1.getName());
        }

        List<String> subjects = new ArrayList<>();
        for (Subject subject1 : Subject.values()) {
            subjects.add(subject1.getName());
        }

        List<String> employmentTypes = new ArrayList<>();
        for (EmploymentType employmentType1 : EmploymentType.values()) {
            employmentTypes.add(employmentType1.getName());
        }

        List<String> weekdays = new ArrayList<>();
        for (Weekdays weekdays1 : Weekdays.values()) {
            weekdays.add(weekdays1.getName());
        }

        return new

                AllDataDTO(groupDTOSet, lessonDTOSet, roomDTOSet, studentDTOSet, trainerDTOSet,
                specializations, subjects, employmentTypes, weekdays);
    }
}

