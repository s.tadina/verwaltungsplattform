package com.example.Verwaltungsplattform.services;

import com.example.Verwaltungsplattform.DTOs.StudentDTO;
import com.example.Verwaltungsplattform.enums.Specializations;
import com.example.Verwaltungsplattform.model.StudentModel;
import com.example.Verwaltungsplattform.repository.GroupRepository;
import com.example.Verwaltungsplattform.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    GroupRepository groupRepository;


    public StudentDTO post(StudentDTO studentDTO) {

        StudentModel studentModel = new StudentModel();


        studentModel.setFirstName(studentDTO.getFirstName());
        studentModel.setLastName(studentDTO.getLastName());
        studentModel.setEmail(studentDTO.getEmail());
        studentModel.setSpecializations(Specializations.stringToEnum(studentDTO.getSpecializations()));
        studentModel.setGroupModel(groupRepository.findById(studentDTO.getGroupId()).orElse(null));

        studentRepository.save(studentModel);

        return new StudentDTO()
                .setId(studentModel.getId())
                .setFirstName(studentModel.getFirstName())
                .setLastName(studentModel.getLastName())
                .setEmail(studentModel.getEmail())
                .setSpecializations(studentModel.getSpecializations() != null ? studentModel.getSpecializations().getName() : "")
                .setGroupId(studentModel.getGroupModel() != null ? studentModel.getGroupModel().getGroupId() : -1);
    }


    public StudentDTO put(StudentDTO studentDTO) {

        StudentModel studentModel = studentRepository.findById(studentDTO.getId()).get();

        studentModel.setFirstName(studentDTO.getFirstName());
        studentModel.setLastName(studentDTO.getLastName());
        studentModel.setEmail(studentDTO.getEmail());
        studentModel.setSpecializations(Specializations.stringToEnum(studentDTO.getSpecializations()));
        studentModel.setGroupModel(groupRepository.findById(studentDTO.getGroupId()).orElse(null));

        studentRepository.save(studentModel);

        return new StudentDTO()
                .setId(studentModel.getId())
                .setFirstName(studentModel.getFirstName())
                .setLastName(studentModel.getLastName())
                .setEmail(studentModel.getEmail())
                .setSpecializations(studentModel.getSpecializations() != null ? studentModel.getSpecializations().getName() : "")
                .setGroupId(studentModel.getGroupModel() != null ? studentModel.getGroupModel().getGroupId() : -1);
    }

    public void delete(int studentId) {
        studentRepository.deleteById(studentId);
    }

}
