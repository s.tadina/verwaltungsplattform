package com.example.Verwaltungsplattform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VerwaltungsplattformApplication {

    public static void main(String[] args) {
        SpringApplication.run(VerwaltungsplattformApplication.class, args);
    }

}
