package com.example.Verwaltungsplattform.model;

import com.example.Verwaltungsplattform.enums.Specializations;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "students")
@Getter
@Setter
public class StudentModel extends PersonModel {

    private Specializations specializations;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private GroupModel groupModel;

    public StudentModel() {
    }

    public StudentModel(String firstName, String lastName, String email) {
        super(firstName, lastName, email);
    }


}
