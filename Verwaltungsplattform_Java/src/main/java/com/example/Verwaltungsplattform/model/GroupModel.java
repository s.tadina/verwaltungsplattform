package com.example.Verwaltungsplattform.model;

import com.example.Verwaltungsplattform.enums.Specializations;
import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity(name = "groups")
public class GroupModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int groupId;

    @Column
    private double groupNumber;

    @Enumerated(EnumType.STRING)
    private Specializations specializations;

    @Column
    private Date startDate;
    @Column
    private Date endDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "room_id")
    private RoomModel roomModel;

    @OneToMany(mappedBy = "groupModel")
    private Set<StudentModel> studentModelSet = new HashSet<>();

    @OneToMany(mappedBy = "groupModel")
    private Set<LessonModel> lessonModelSet = new HashSet<>();


    public GroupModel(int groupNumber, Date startDate, Date endDate, String specializations) {
        this.groupNumber = groupNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.specializations = Specializations.stringToEnum(specializations);
    }


    public GroupModel() {
    }

    public void addLessonSet(LessonModel lessonModel) {
        lessonModelSet.add(lessonModel);
    }

    public void addStudentSet(StudentModel studentModel) {
        studentModelSet.add(studentModel);
    }


}



