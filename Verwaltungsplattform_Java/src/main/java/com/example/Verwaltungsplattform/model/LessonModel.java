package com.example.Verwaltungsplattform.model;

import com.example.Verwaltungsplattform.DTOs.LessonDTO;
import com.example.Verwaltungsplattform.enums.Subject;
import com.example.Verwaltungsplattform.enums.Weekdays;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity(name = "lessons")
public class LessonModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int lessonId;

    @ManyToOne
    @JoinColumn(name = "trainer_id")
    private TrainerModel trainerModel;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private GroupModel groupModel;

    @Enumerated(EnumType.STRING)
    private Subject subjects;

    @Enumerated(EnumType.STRING)
    private Weekdays weekday;

    public LessonModel() {
    }

    public LessonModel(TrainerModel trainerModel, GroupModel groupModel, Subject subjects, Weekdays weekday) {
        this.trainerModel = trainerModel;
        this.groupModel = groupModel;
        this.subjects = subjects;
        this.weekday = weekday;
    }
}



