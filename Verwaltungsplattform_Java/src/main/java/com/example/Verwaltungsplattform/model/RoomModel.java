package com.example.Verwaltungsplattform.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "rooms")
public class RoomModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int roomId;

    @Column
    private int roomNumber;
    @Column
    private String vip;
    @Column
    private String descriptionRoom;

    @OneToOne(mappedBy = "roomModel")
    private GroupModel groupModel;

    public RoomModel() {
    }

    public RoomModel(int roomNumber, String vip, String descriptionRoom, GroupModel groups) {
        this.roomNumber = roomNumber;
        this.descriptionRoom = descriptionRoom;
        this.vip = vip;
        this.groupModel = groups;
    }
}
