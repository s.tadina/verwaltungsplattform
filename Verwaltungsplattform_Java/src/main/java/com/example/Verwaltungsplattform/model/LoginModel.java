package com.example.Verwaltungsplattform.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Getter
@Setter
@Accessors(chain = true)

public class LoginModel {
    @Id
    String username;
    int password;


}
