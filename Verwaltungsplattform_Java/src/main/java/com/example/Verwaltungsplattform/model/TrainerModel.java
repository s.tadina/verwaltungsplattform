package com.example.Verwaltungsplattform.model;

import com.example.Verwaltungsplattform.enums.EmploymentType;
import com.example.Verwaltungsplattform.enums.Subject;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter

public class TrainerModel extends PersonModel {
    @Column
    private String residence;
    @Column
    private double grossSalary;
    @Column
    private double netSalary;

    @Column
    @ElementCollection
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "TrainerSubject")
    Set<Subject> subject = new HashSet<>();

    @Enumerated(EnumType.STRING)
    EmploymentType employmentType;

    @OneToMany(mappedBy = "trainerModel")
    private Set<LessonModel> lessonModel = new HashSet<>();

    public TrainerModel() {
    }

    public TrainerModel(String firstName, String lastName, String email, String residence, double grossSalary, double
            netSalary, String employmentType) {
        super(firstName, lastName, email);
        this.residence = residence;
        this.grossSalary = grossSalary;
        this.netSalary = netSalary;
        this.employmentType = EmploymentType.stringToEnum(employmentType);
    }

    public void addSubject(Subject subject) {
        this.subject.add(subject);
    }

    public void addLessonSet(LessonModel lesson) {
        lessonModel.add(lesson);
    }


}


