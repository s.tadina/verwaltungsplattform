package com.example.Verwaltungsplattform.DTOs;

import lombok.*;
import lombok.experimental.Accessors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class StudentDTO {
    private String firstName, lastName, email;
    private String specializations;
    private int id, groupId;
}
