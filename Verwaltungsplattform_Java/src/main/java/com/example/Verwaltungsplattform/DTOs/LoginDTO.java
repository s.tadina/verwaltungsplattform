package com.example.Verwaltungsplattform.DTOs;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)

public class LoginDTO {
    String username;
    String password;


}
