package com.example.Verwaltungsplattform.DTOs;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class LessonDTO {

    private int lessonId;
    private int trainerId;
    private int groupId;
    private String subjects;
    private String weekday;

}
