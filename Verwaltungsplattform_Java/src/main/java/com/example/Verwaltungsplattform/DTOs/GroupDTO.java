package com.example.Verwaltungsplattform.DTOs;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class GroupDTO {


    private int groupId;
    private double groupNumber;
    private String specializations;
    private int roomId;
    private Date startDate, endDate;


    private Set<Integer> lessonIdSet = new HashSet<>();
    private Set<Integer> studentIdSet = new HashSet<>();

}
