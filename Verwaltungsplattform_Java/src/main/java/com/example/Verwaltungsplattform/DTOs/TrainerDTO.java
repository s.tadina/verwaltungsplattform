package com.example.Verwaltungsplattform.DTOs;

import java.util.Set;

import lombok.*;
import lombok.experimental.Accessors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TrainerDTO {

    private String firstName, lastName, email, residence, employmentType;
    private double salary;
    private Set<Integer> lessonIdSet;
    private int id;
    private Set<String> subjects;


}
