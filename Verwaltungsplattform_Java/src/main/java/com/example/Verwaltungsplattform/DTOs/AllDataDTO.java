package com.example.Verwaltungsplattform.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;
import java.util.List;

@Data
@AllArgsConstructor
public class AllDataDTO {

    private Set<GroupDTO> groupDTOSet;
    private Set<LessonDTO> lessonDTOSet;
    private Set<RoomDTO> roomDTOSet;
    private Set<StudentDTO> studentDTOSet;
    private Set<TrainerDTO> trainerDTOSet;


    private List<String> specializations;
    private List<String> subjects;
    private List<String> employmentTypes;
    private List<String> weekdays;
}
