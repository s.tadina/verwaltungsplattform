package com.example.Verwaltungsplattform.DTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class RoomDTO {
    private int roomId;
    private int roomNumber;
    private String descriptionRoom, vip;
    private int groupId;
}
