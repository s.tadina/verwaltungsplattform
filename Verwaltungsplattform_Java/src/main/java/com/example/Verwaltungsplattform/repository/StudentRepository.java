package com.example.Verwaltungsplattform.repository;

import com.example.Verwaltungsplattform.model.StudentModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<StudentModel, Integer> {
}
