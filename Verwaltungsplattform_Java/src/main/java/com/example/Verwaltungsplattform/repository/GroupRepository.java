package com.example.Verwaltungsplattform.repository;

import com.example.Verwaltungsplattform.model.GroupModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<GroupModel, Integer> {
}
