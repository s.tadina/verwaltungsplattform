package com.example.Verwaltungsplattform.repository;

import com.example.Verwaltungsplattform.model.TrainerModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainerRepository extends CrudRepository<TrainerModel, Integer> {
}
