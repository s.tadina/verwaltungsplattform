package com.example.Verwaltungsplattform.repository;

import com.example.Verwaltungsplattform.model.LoginModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoginRepository extends CrudRepository<LoginModel, String> {

}
