package com.example.Verwaltungsplattform.enums;

public enum Specializations {

    SOFTWARE("Software"),
    NETWORK("Netzwerktechnik"),
    FIA("FIA"),
    QUALIFYING("Qualifying"),

    NOTDEFINED("Nicht definiert");

    private final String name;

    Specializations(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public static Specializations stringToEnum(String specialization) {
        for (Specializations specializations : Specializations.values()) {
            if (specializations.name.equals(specialization)) {
                return specializations;
            }
        }
        return Specializations.NOTDEFINED;
    }
}






