package com.example.Verwaltungsplattform.enums;

public enum EmploymentType {
    FULLTIME("Vollzeit"),
    PARTTIME("Teilzeit"),
    SELFEMPLOYED("Selbstständig"),
    NOTDEFINED("Nicht Definiert");

    private final String name;

    EmploymentType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static EmploymentType stringToEnum(String type) {
        if (type != null) {
            for (EmploymentType employmentType : EmploymentType.values()) {
                if (employmentType.getName().equals(type))
                    return employmentType;
            }
        }
        return EmploymentType.NOTDEFINED;
    }
}

