package com.example.Verwaltungsplattform.enums;

public enum Subject {

    JAVA("Java"),
    WEB("Web"),
    DATABASE("Datenbanken"),
    CAREER("Karrierecoaching"),
    NETWORK("Netzwerktechnik"),
    LINUX("Linux"),
    WINDOWS("Windows"),
    NONE("Keine");

    private final String name;

    Subject(String name) {
        this.name = name;
    }

    public static Subject stringToSubject(String subject) {
        for (Subject subject1 : Subject.values()) {
            if (subject1.getName().equals(subject)) {
                return subject1;

            }
        }
        return Subject.NONE;
    }

    public String getName() {
        return name;
    }
}


