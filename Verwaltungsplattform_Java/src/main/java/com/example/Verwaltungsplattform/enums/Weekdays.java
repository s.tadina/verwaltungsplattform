package com.example.Verwaltungsplattform.enums;

public enum Weekdays {
    MONDAY("Montag"),
    TUESDAY("Dienstag"),
    WEDNESDAY("Mittwoch"),
    THURSDAY("Donnerstag"),
    FRIDAY("Freitag"),
    NOTDEFINED("Nicht definiert");

    private final String name;

    Weekdays(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Weekdays stringToEnum(String weekday) {

        for (Weekdays weekdays : Weekdays.values()) {
            if (weekdays.getName().equals(weekday)) {
                return weekdays;
            }
        }
        return Weekdays.NOTDEFINED;
    }
}


