package com.example.Verwaltungsplattform.controller;

import com.example.Verwaltungsplattform.DTOs.LoginDTO;
import com.example.Verwaltungsplattform.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/login")

public class LoginController {

    final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping
    public ResponseEntity createLogin(@RequestBody LoginDTO loginDTO) {
        if (loginService.createLogin(loginDTO)) {
            return new ResponseEntity(HttpStatus.CREATED);
        } else
            return ResponseEntity.badRequest().body("Bad");
    }

    @PutMapping
    public ResponseEntity login(@RequestBody LoginDTO loginDTO) {
        if (loginService.login(loginDTO))
            return ResponseEntity.ok("");
        else
            return ResponseEntity.badRequest().body("BAAD");
    }
}
