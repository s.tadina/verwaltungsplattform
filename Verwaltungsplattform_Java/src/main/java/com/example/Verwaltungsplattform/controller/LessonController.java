package com.example.Verwaltungsplattform.controller;

import com.example.Verwaltungsplattform.DTOs.LessonDTO;
import com.example.Verwaltungsplattform.model.LessonModel;
import com.example.Verwaltungsplattform.services.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/lessons")
public class LessonController {

    @Autowired
    LessonService lessonService;


    @CrossOrigin
    @PostMapping
    public LessonDTO postLesson(@RequestBody LessonDTO lessonDTO) {
        return lessonService.post(lessonDTO);
    }

    @CrossOrigin
    @PutMapping("{lessonId}")
    public LessonDTO putLesson(@RequestBody LessonDTO lessonDTO, @PathVariable int lessonId) {
        return lessonService.put(lessonDTO, lessonId);
    }

    @CrossOrigin
    @DeleteMapping("{lessonId}")
    public void deleteLesson(@PathVariable int lessonId) {
        lessonService.delete(lessonId);
    }


}
