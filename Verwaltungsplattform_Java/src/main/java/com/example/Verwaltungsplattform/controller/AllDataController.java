package com.example.Verwaltungsplattform.controller;

import com.example.Verwaltungsplattform.DTOs.AllDataDTO;
import com.example.Verwaltungsplattform.services.AllDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/alldata")
public class AllDataController {
    @Autowired
    AllDataService allDataService;

    @GetMapping
    public AllDataDTO getAllData() {
        return allDataService.getAllData();
    }
}

