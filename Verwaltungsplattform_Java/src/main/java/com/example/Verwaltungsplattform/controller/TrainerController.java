package com.example.Verwaltungsplattform.controller;

import com.example.Verwaltungsplattform.DTOs.TrainerDTO;
import com.example.Verwaltungsplattform.model.TrainerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.Verwaltungsplattform.services.TrainerService;


@RestController
@RequestMapping("api/trainers")
public class TrainerController {

    @Autowired
    TrainerService trainerService;

    @CrossOrigin
    @PostMapping
    public TrainerDTO postTrainer(@RequestBody TrainerDTO trainerDTO) {
        return trainerService.post(trainerDTO);

    }

    @CrossOrigin
    @PutMapping("{trainerId}")
    public TrainerDTO putTrainer(@RequestBody TrainerDTO trainerDTO) {
        return trainerService.put(trainerDTO);
    }

    @CrossOrigin
    @DeleteMapping("{trainerId}")
    public void deleteTrainer(@PathVariable int trainerId) {
        trainerService.delete(trainerId);
    }
}
