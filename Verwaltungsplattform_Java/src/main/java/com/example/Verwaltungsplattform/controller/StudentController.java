package com.example.Verwaltungsplattform.controller;

import com.example.Verwaltungsplattform.DTOs.StudentDTO;
import com.example.Verwaltungsplattform.model.StudentModel;
import com.example.Verwaltungsplattform.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/students")
public class StudentController {


    @Autowired
    StudentService studentService;

    @CrossOrigin
    @PostMapping
    public StudentDTO postStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.post(studentDTO);
    }

    @CrossOrigin
    @PutMapping("/{studentId}")
    public StudentDTO putStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.put(studentDTO);
    }

    @CrossOrigin
    @DeleteMapping("/{studentId}")
    public void deleteStudent(@PathVariable int studentId) {
        studentService.delete(studentId);
    }


}