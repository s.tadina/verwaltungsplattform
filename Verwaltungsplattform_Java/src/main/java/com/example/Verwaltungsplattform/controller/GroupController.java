package com.example.Verwaltungsplattform.controller;

import com.example.Verwaltungsplattform.DTOs.GroupDTO;
import com.example.Verwaltungsplattform.model.GroupModel;
import com.example.Verwaltungsplattform.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;

@RestController
@RequestMapping("api/groups")
public class GroupController {

    @Autowired
    GroupService groupService;

    @CrossOrigin
    @PostMapping
    public GroupDTO postGroup(@RequestBody GroupDTO groupDTO) {
        return groupService.post(groupDTO);
    }

    @CrossOrigin
    @PutMapping("{groupId}")
    public GroupDTO putGroup(@RequestBody GroupDTO groupDTO, int groupId) {
        return groupService.put(groupDTO, groupId);
    }

    @CrossOrigin
    @DeleteMapping("{groupId}")
    public void deleteGroup(@PathVariable int groupId) {
        groupService.delete(groupId);
    }
}
