package com.example.Verwaltungsplattform.controller;


import com.example.Verwaltungsplattform.DTOs.RoomDTO;
import com.example.Verwaltungsplattform.model.RoomModel;
import com.example.Verwaltungsplattform.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/rooms")
public class RoomController {

    @Autowired
    RoomService roomService;


    @CrossOrigin
    @PostMapping
    public RoomDTO postRoom(@RequestBody RoomDTO roomDTO) {
        return roomService.post(roomDTO);
    }

    @CrossOrigin
    @PutMapping("{roomId}")
    public RoomDTO putRoom(@RequestBody RoomDTO roomDTO, @PathVariable int roomId) {
        return roomService.put(roomDTO, roomId);
    }

    @CrossOrigin
    @DeleteMapping("{roomId}")
    public void deleteRoom(@PathVariable int roomId) {
        roomService.delete(roomId);
    }


}
